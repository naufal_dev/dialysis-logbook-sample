import React from 'react';
import { useSelector } from 'react-redux';
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Dashboard from '../screens/Dashboard';
import { Text, View } from 'native-base';
import BloodValues from '../screens/BloodValues';
import AddWeight from '../screens/AddWeight';
import AddWellbeing from '../screens/AddWellbeing';
import AddItchIntensity from '../screens/AddItchIntensity';
import ViewData from '../screens/viewData';

const Navigator = createNativeStackNavigator();

const StackNavigator = () => {
    return (
        <Navigator.Navigator initialRouteName="Dashboard">
            <Navigator.Screen name="Dashboard" component={Dashboard} />
            <Navigator.Screen name="BloodValues" component={BloodValues} />
            <Navigator.Screen name="Weight" component={AddWeight} />
            <Navigator.Screen name="Wellbeing" component={AddWellbeing} />
            <Navigator.Screen name="Itch Intensity" component={AddItchIntensity} />
            <Navigator.Screen name="ViewData" component={ViewData} />
        </Navigator.Navigator>
    );
};

export default StackNavigator;
