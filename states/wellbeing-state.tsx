import { createSlice, PayloadAction } from "@reduxjs/toolkit";

/**
 * TODO:
 * Manage redux reducers here and add them to the logbookSlice
 * The reducer is already added to the store.ts file
 */

const wellbeingSlice = createSlice({
  name: "wellbeing",
  initialState: {
    value: []
  },
  reducers: {
    logWellbeingEntry: (state, action) => {
      state.value.push(action.payload);
    },
  },
});
export const { logWellbeingEntry } = wellbeingSlice.actions
export const wellbeingReducer = wellbeingSlice.reducer;
