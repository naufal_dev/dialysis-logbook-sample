import { createSlice, PayloadAction } from "@reduxjs/toolkit";

/**
 * TODO:
 * Manage redux reducers here and add them to the logbookSlice
 * The reducer is already added to the store.ts file
 */

const bloodValuesSlice = createSlice({
  name: "bloodvalues",
  initialState: {
    value: []
  },
  reducers: {
    logBloodValuesEntry: (state, action) => {
      state.value.push(action.payload);
    },
  },
});
export const { logBloodValuesEntry } = bloodValuesSlice.actions
export const bloodValuesReducer= bloodValuesSlice.reducer;
