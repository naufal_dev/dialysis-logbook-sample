import { createSlice, PayloadAction } from "@reduxjs/toolkit";

/**
 * TODO:
 * Manage redux reducers here and add them to the logbookSlice
 * The reducer is already added to the store.ts file
 */

const weightSlice = createSlice({
  name: "weight",
  initialState: {
    value: []
  },
  reducers: {
    logWeightEntry: (state, action) => {
      state.value.push(action.payload);
    },
  },
});
export const { logWeightEntry } = weightSlice.actions
export const weightReducer = weightSlice.reducer;
