import { createSlice, PayloadAction } from "@reduxjs/toolkit";

/**
 * TODO:
 * Manage redux reducers here and add them to the logbookSlice
 * The reducer is already added to the store.ts file
 */

const itchIntensitySlice = createSlice({
  name: "itchIntensity",
  initialState: {
    value: []
  },
  reducers: {
    logItchIntensityEntry: (state, action) => {
      state.value.push(action.payload);
    },
  },
});
export const { logItchIntensityEntry } = itchIntensitySlice.actions
export const itchIntensityReducer = itchIntensitySlice.reducer;
