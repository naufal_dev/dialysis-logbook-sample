import { createSlice, PayloadAction } from "@reduxjs/toolkit";

/**
 * TODO:
 * Manage redux reducers here and add them to the logbookSlice
 * The reducer is already added to the store.ts file
 */

const logbookSlice = createSlice({
  name: "logbook",
  initialState: {
    value: 0,
  },
  reducers: {
    incrementByAmount: (state, action: PayloadAction<number>) => {
      state.value += action.payload;
    },
  },
});

export const logbookReducer = logbookSlice.reducer;
