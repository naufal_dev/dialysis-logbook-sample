import React, { useState, useEffect, useRef } from 'react';
import { Box, Button, Input, Text, View, VStack, } from 'native-base';
import { useSelector, useDispatch } from 'react-redux'
import { logBloodValuesEntry } from '../../states/bloodValues-state';


const BloodValues = ({ }) => {
    const [potassiumValue, setPotassiumValue] = useState('')
    const [phosphateValue, setPhosphateValue] = useState('')
    const [potassiumErr, setPotassiumErr] = useState('')
    const [phosphateErr, setPhosphateErr] = useState('')
    const dispatch = useDispatch()
    const bloodValuesState = useSelector(state => state.bloodValues.value)

    return (
        <View>
            <Box alignItems="center">
                <Text>POTASSIUM</Text>
                <Input mx="3" placeholder="Input" w="100%" keyboardType="numeric" value={potassiumValue} onChangeText={(value) => {
                    setPotassiumValue(value)
                }} />
                {potassiumErr ? <Text>{potassiumErr}</Text> : null}

                <Text>PHOSPHATE</Text>
                <Input mx="3" placeholder="Input" w="100%" keyboardType="numeric" value={phosphateValue} onChangeText={(value) => {
                    setPhosphateValue(value)
                }} />
                {phosphateErr ? <Text>{phosphateErr}</Text> : null}
            </Box>
            <Button onPress={() => { dispatch(logBloodValuesEntry({ value: { potassiumValue, phosphateValue }, time: new Date().getTime().toString() })) }}>Save</Button>
        </View>
    );
};

export default BloodValues;
