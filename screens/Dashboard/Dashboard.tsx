import React, { useState, useEffect, useRef } from 'react';
import { Box, Button, Stack, Text, View, VStack, } from 'native-base';


const Dashboard = ({ navigation }) => {

    return (
        <View>
            <VStack w="100%" h='100%' space={4} px="2" mt="4" alignItems="center" justifyContent="center">
                <Button onPress={() => navigation.navigate('BloodValues')}>Blood Values</Button>
                <Button onPress={() => navigation.navigate('Weight')}>Weight</Button>
                <Button onPress={() => navigation.navigate('Wellbeing')}>Wellbeing</Button>
                <Button onPress={() => navigation.navigate('Itch Intensity')}>Itch intensity</Button>
                <Button onPress={() => navigation.navigate('ViewData')}>View Data</Button>
            </VStack>

        </View>
    );
};

export default Dashboard;
