import React, { useState, useEffect, useRef } from 'react';
import { Box, Button, CheckIcon, Input, Select, Slider, Text, View, VStack, } from 'native-base';
import { useSelector, useDispatch } from 'react-redux'
import { logWellbeingEntry } from '../../states/wellbeing-state';

const AddWellbeing = ({ }) => {
    const dispatch = useDispatch()
    const wellbeingState = useSelector(state => { return state.wellbeing.value })
    const [wellbeing, setWellbeing] = useState(3)

    return (
        <VStack space={4}>
            <Box alignItems="center" >
                <Text>{wellbeing}</Text>
                <Slider w="3/4" maxW="300" defaultValue={3} minValue={1} maxValue={6} accessibilityLabel="Wellbeing" step={1}
                    onChange={v => {
                        setWellbeing(Math.floor(v));
                    }}>
                    <Slider.Track>
                        <Slider.FilledTrack />
                    </Slider.Track>
                    <Slider.Thumb />
                </Slider>
            </Box>
            <Button onPress={() => { dispatch(logWellbeingEntry({ value: wellbeing, time: new Date().getTime().toString() })) }}> Save</Button >

        </VStack >
    );
};

export default AddWellbeing;
