import React, { useState, useEffect, useRef } from 'react';
import { Box, Button, CheckIcon, Input, Select, Slider, Text, View, VStack, } from 'native-base';
import { useSelector, useDispatch } from 'react-redux'
import { logItchIntensityEntry } from '../../states/itchIntensity-state';


const ItchIntensity = ({ }) => {
    const [itchIntensity, setItchIntensity] = useState(5)
    const dispatch = useDispatch()
    const itchIntensityState = useSelector(state => state.itchIntensity.value)

    return (
        <VStack space={4}>
            <Box alignItems="center" >
                <Text>{itchIntensity}</Text>
                <Slider w="3/4" maxW="300" defaultValue={5} minValue={1} maxValue={10} accessibilityLabel="Itch Intensity" step={1}
                    onChange={v => {
                        setItchIntensity(Math.floor(v));
                    }}>
                    <Slider.Track>
                        <Slider.FilledTrack />
                    </Slider.Track>
                    <Slider.Thumb />
                </Slider>
            </Box>
            <Button onPress={() => { dispatch(logItchIntensityEntry({ value: itchIntensity, time: new Date().getTime().toString() })) }}  >Save</Button>

        </VStack >
    );
};

export default ItchIntensity;
