import React, { useState, useEffect, useRef } from 'react';
import { Box, Button, CheckIcon, Input, Select, Text, View, VStack, } from 'native-base';
import { useSelector, useDispatch } from 'react-redux'
import { logWeightEntry } from '../../states/weight-state';

const AddWeight = ({ }) => {
    const [weight, setWeight] = useState('')
    const [weightErr, setWeightErr] = useState('')
    const [measurementTime, setMeasurementTime] = useState('')
    const dispatch = useDispatch()
    const weightState = useSelector(state => { return state.weight.value })

    
    return (
        <View>
            <Box alignItems="center">
                <Select minWidth="200" accessibilityLabel="Time of measurement" placeholder="Time of measurement" _selectedItem={{
                    bg: "teal.600",
                    endIcon: <CheckIcon size="5" />
                }} mt={1}
                    onValueChange={value => setMeasurementTime(value)}
                >
                    <Select.Item label="Before Dialysis" value="Before Dialysis" />
                    <Select.Item label="After Dialysis" value="After Dialysis" />
                    <Select.Item label="Arbitrary" value="Arbitrary" />
                </Select>
                <Text>Weight</Text>
                <Input mx="3" placeholder="Input" w="100%"
                    keyboardType="numeric"
                    value={weight}
                    onChangeText={(value) => {
                        setWeight(value)
                    }} />
                {weightErr ? <Text>{weightErr}</Text> : null}
            </Box>
            <Button onPress={() => { dispatch(logWeightEntry({ value: { weight, measurementTime }, time: new Date().getTime().toString() })) }}>Save</Button>

        </View >
    );
};

export default AddWeight;
