import React, { useState, useEffect, useRef } from 'react';
import { Box, Button, Center, Divider, HStack, Icon, Input, Text, View, VStack, } from 'native-base';
import { useSelector, useDispatch } from 'react-redux'


const ViewData = ({ }) => {
    const bloodValuesState = useSelector(state => state.bloodValues.value);
    const weightValuesState = useSelector(state => state.weight.value);
    const wellBeingValuesState = useSelector(state => state.wellbeing.value);
    const itchIntecityValuesState = useSelector(state => state.itchIntensity.value);


    const [PottasiumItems, setPottasiumItems] = useState([]);
    const [PhospateItems, setPhospateItems] = useState([]);
    const [IndexItems, setIndexItems] = useState([]);

    const [weightIndexItems, setWeightIndexItems] = useState([]);
    const [weightItems, setWeightItems] = useState([]);
    const [timeOfWeightItems, setTimeOfWeightItems] = useState([]);

    const [itchIntencityIndexItems, setItchIntencityIndexItems] = useState([]);
    const [itchIntencityItems, setItchIntencityItems] = useState([]);

    const [wellbeingIndexItems, setWellBeingIndexItems] = useState([]);
    const [wellbeingItems, setWellBeingItems] = useState([]);

    useEffect(() => {
        let tempPotassiumItems = [];
        let tempPhospateItems = [];
        let tempIndexItems = [];

        let tempTimeOfWeightMeasurementValue = [];
        let tempWeightValue = [];
        let tempWeightIndex=[];

        let tempWellbeingIndexItems = [];
        let tempWellbeingValueItems = [];

        let tempItchIntencityIndexItems = [];
        let tempItchIntencityValueItems = [];

        bloodValuesState.forEach((item, index) => {
            tempPotassiumItems.push(<Text>{item.value.phosphateValue}</Text>);
            tempPhospateItems.push(<Text>{item.value.potassiumValue}</Text>);
            tempIndexItems.push(<Text>{index+1}</Text>);
        })

        weightValuesState.forEach((item, index) => {
            tempTimeOfWeightMeasurementValue.push(<Text>{item.value.measurementTime}</Text>);
            tempWeightValue.push(<Text>{item.value.weight}</Text>);
            tempWeightIndex.push(<Text>{index+1}</Text>);
        })

        wellBeingValuesState.forEach((item, index) => {
            tempWellbeingValueItems.push(<Text>{item.value}</Text>);
            tempWellbeingIndexItems.push(<Text>{index+1}</Text>);
        })

        itchIntecityValuesState.forEach((item, index) => {
            tempItchIntencityValueItems.push(<Text>{item.value}</Text>);
            tempItchIntencityIndexItems.push(<Text>{index+1}</Text>);
        })


        setPottasiumItems(tempPotassiumItems)
        setPhospateItems(tempPhospateItems)
        setIndexItems(tempIndexItems)

        setWeightIndexItems(tempWeightIndex)
        setWeightItems(tempWeightValue)
        setTimeOfWeightItems(tempTimeOfWeightMeasurementValue)

        setItchIntencityIndexItems(tempItchIntencityIndexItems)
        setItchIntencityItems(tempItchIntencityValueItems)

        setWellBeingIndexItems(tempWellbeingIndexItems)
        setWellBeingItems(tempWellbeingValueItems)
    }, [])
    return (
        <View>
            <Text>Blood Values</Text>
            <HStack space={3} justifyContent="center">
                <VStack>
                    <Text>Sr.No</Text>
                    {IndexItems}
                </VStack>
                <VStack>
                    <Text>Pottasium</Text>
                    {PottasiumItems}
                </VStack>
                <VStack>
                    <Text>Phospate</Text>
                    {PhospateItems}
                </VStack>
            </HStack>

            <Text>Weight</Text>
            <HStack space={3} justifyContent="center">
                <VStack>
                    <Text>Sr.No</Text>
                    {weightIndexItems}
                </VStack>
                <VStack>
                    <Text>Time Of Weight</Text>
                    {timeOfWeightItems}
                </VStack>
                <VStack>
                    <Text>Weight</Text>
                    {weightItems}
                </VStack>
            </HStack>

            <Text>Well Being value</Text>
            <HStack space={3} justifyContent="center">
                <VStack>
                    <Text>Sr.No</Text>
                    {wellbeingIndexItems}
                </VStack>
                <VStack>
                    <Text>Well Being</Text>
                    {wellbeingItems}
                </VStack>
            </HStack>

            <Text>Itch Intencity value</Text>
            <HStack space={3} justifyContent="center">
                <VStack>
                    <Text>Sr.No</Text>
                    {itchIntencityIndexItems}
                </VStack>
                <VStack>
                    <Text>Itch Intencity</Text>
                    {itchIntencityItems}
                </VStack>
            </HStack>
        </View>
    );
};

export default ViewData;
