import { configureStore } from "@reduxjs/toolkit";
import { logbookReducer } from "./logbook-state";
import { wellbeingReducer } from "./states/wellbeing-state";
import { itchIntensityReducer } from "./states/itchIntensity-state";
import { bloodValuesReducer } from "./states/bloodValues-state";
import { weightReducer } from "./states/weight-state";

export default configureStore({
  reducer: {
    logbook: logbookReducer,
    wellbeing: wellbeingReducer,
    itchIntensity: itchIntensityReducer,
    bloodValues: bloodValuesReducer,
    weight: weightReducer
  },
});
